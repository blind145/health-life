package epis.unsa;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import epis.unsa.beans.Admi;

public class AdmiDatos {

	
	public static void insertar(String nombre, String apellidop, String apellidom, String fechanac, String estado,
			  String sexo, String dni, String direccion,
			 String lugar, String telefono, String email){
	 
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	
	 final Admi admi = new Admi(nombre, apellidop, apellidom, fechanac, estado,
			  sexo, dni, direccion,
			 lugar, telefono, email);
	
	 pm.makePersistent(admi);
	 pm.close();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Admi> verAdmis(){
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Query query = pm.newQuery(Admi.class);
	 return (List<Admi>) query.execute();
	 
	}
}
