package epis.unsa;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.jdo.Query;
import javax.servlet.RequestDispatcher;

import epis.unsa.beans.Especialidad;


@SuppressWarnings("serial")
public class BorrarEspecialidad extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	try {
			if (request.getParameter("action").equals("borrar")) {
				
				String nombre = request.getParameter("nombre");
				PersistenceManager pm = PMF.get().getPersistenceManager();
				
				final Query q = pm.newQuery(Especialidad.class);
				q.setFilter("nombre == nombreParam");
				q.declareParameters("String nombreParam");
				
				try{
					List<Especialidad> especialidades = (List<Especialidad>) q.execute(nombre);

					for(Especialidad p: especialidades){
						
						pm.deletePersistent(p);
					}
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Confirmar.jsp");
					rd.forward(request, response);

				}catch(Exception e){
					
				}finally{
					 q.closeAll();
				}
			
			}	
	
	} catch (Exception e) {
		RequestDispatcher rd = getServletContext()
				.getRequestDispatcher("/WEB-INF/Error.jsp");
		rd.forward(request, response);
		}
	
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}


