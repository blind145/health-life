package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;

import epis.unsa.beans.Historia;

@SuppressWarnings("serial")
public class BuscarHistorias extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("action").equals("Busqueda por DNI")) {
				String dni = request.getParameter("dni");
				 request.setAttribute("dni", dni);
			     List<Historia> historias = Busqueda.buscardni(dni);
			     request.setAttribute("historias", historias);
			     request.setAttribute("dni", dni);
			     RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/busquedaDNI.jsp");
			     rd.forward(request, response);

			} else if (request.getParameter("action").equals("Busqueda por medico")) {
				
				String medico = request.getParameter("medico");
				 request.setAttribute("medico", medico);
			     List<Historia> historias = Busqueda.buscarmedico(medico);
			     request.setAttribute("historias", historias);
			     request.setAttribute("medico", medico);
			     RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/busquedaMedico.jsp");
			     rd.forward(request, response);
			     
			} else if (request.getParameter("action").equals("Busqueda por especialidad")) {
				
				String especialidad = request.getParameter("especialidad");
				 request.setAttribute("especialidad", especialidad);
			     List<Historia> historias = Busqueda.buscarespecialidad(especialidad);
			     request.setAttribute("historias", historias);
			     request.setAttribute("especialidad", especialidad);
			     RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/busquedaEspecialidad.jsp");
			     rd.forward(request, response);

			}else if (request.getParameter("action").equals("mapa")) {
				
			     RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/siteMap.jsp");
			     rd.forward(request, response);

			}
			
		} catch (Exception e) {
			 RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/buscarHistoria.jsp");
		     rd.forward(request, response);

		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
