package epis.unsa;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import epis.unsa.beans.Especialidad;


public class EspecialidadDatos {


	public static void insertar(String nombre, String turno, String tipo){
	
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Especialidad especialidad = new Especialidad(nombre, turno, tipo);
	 pm.makePersistent(especialidad);
	}
	
	@SuppressWarnings("unchecked")
	public static List<Especialidad> verEspecialidades(){
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Query query = pm.newQuery(Especialidad.class);
	 return (List<Especialidad>) query.execute();
	}
}
