package epis.unsa;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import epis.unsa.beans.Historia;

public class HistoriaDatos {


	public static void insertar(String numero, String interno,String dni,String fecha,String talla,String peso,String persona,
			String relacion,String medico,String especialidad, String her,String pat,String adiccion,String descripcion, String alimentacion,
			String deportes,String inmune,String alergias,String padec,String sintoma,String diges,String cardio,String resp,
			String ur,String gen,String hema,String endo,String osteo,String nerv,String sens,String psico,String anteriores,
			String terapia,String fc,String fr,String ta,String t,String exploracion,String cabeza,String cuello,String torax,
			String abdomen,String miembros,String genes,String comentario,String diagnostico,String pronostico,String tratamiento){

	 final PersistenceManager pm = PMF.get().getPersistenceManager();

	 final Historia historia = new Historia(numero, interno, dni, fecha, talla, peso, persona, relacion, medico, especialidad,
				her, pat, adiccion, descripcion,  alimentacion, deportes, inmune, alergias, padec, sintoma, diges, cardio, resp, ur, 
				gen, hema, endo, osteo, nerv, sens, psico, anteriores, terapia, fc, fr, ta, t, exploracion, cabeza,
				cuello, torax, abdomen, miembros, genes, comentario, diagnostico, pronostico, tratamiento);
	
	 pm.makePersistent(historia);
	}

	@SuppressWarnings("unchecked")
	public static List<Historia> verHistorias(){
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Query query = pm.newQuery(Historia.class);
	 return (List<Historia>) query.execute();
	}
}
