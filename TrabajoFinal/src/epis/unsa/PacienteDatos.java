package epis.unsa;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import epis.unsa.beans.Paciente;

public class PacienteDatos {

	public static void insertar(String nombre, String apellidop, String apellidom, String fechanac, String estado,
			 String grupo, String sexo, String ocupacion, String dni, String direccion,
			 String lugar, String telefono, String email){
	
	 final PersistenceManager pm = PMF.get().getPersistenceManager();

	 final Paciente paciente = new Paciente(nombre, apellidop, apellidom, fechanac, estado,
			 grupo, sexo, ocupacion, dni, direccion,
			 lugar, telefono, email);
	
	 pm.makePersistent(paciente);
	 pm.close();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Paciente> verPacientes(){
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Query query = pm.newQuery(Paciente.class);
	 return (List<Paciente>) query.execute();
	}
}
