package epis.unsa;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import epis.unsa.beans.Receta;

public class RecetaDatos {

	public static void insertar(String numero, String numeroc, String paciente, String fecha, String rp, 
			String indic, String medico){
	
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Receta receta = new Receta(numero, numeroc, paciente, fecha, rp, indic, medico);
	 pm.makePersistent(receta);
	 pm.close();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Receta> verReceta(){
	 final PersistenceManager pm = PMF.get().getPersistenceManager();
	 final Query query = pm.newQuery(Receta.class);
	 return (List<Receta>) query.execute();
	}
}
