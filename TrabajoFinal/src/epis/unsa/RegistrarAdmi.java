package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import epis.unsa.beans.Admi;

@SuppressWarnings("serial")
public class RegistrarAdmi extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("action").equals("create")) {

				String nombre = request.getParameter("nombre");
				String apellidop = request.getParameter("apellidop");
				String apellidom = request.getParameter("apellidom");
				String dia = request.getParameter("dia");
				String mes = request.getParameter("mes");
				String anio = request.getParameter("anio");
				String fechanac = dia + " / " + mes + " / " + anio;
				String estado = request.getParameter("estado");
				String sexo = request.getParameter("sexo");
				String dni = request.getParameter("dni");
				String direccion = request.getParameter("direccion");
				String numero = request.getParameter("numero");
				String distrito = request.getParameter("distrito");
				String provincia = request.getParameter("provincia");
				String departamento = request.getParameter("departamento");
				String direccionp = direccion + " - " + numero + " / "
						+ distrito + " / " + provincia + " / " + departamento;
				String lugar = request.getParameter("lugar");
				String telefono = request.getParameter("telefono");
				String email = request.getParameter("email");
				/*
				List<Admi> admi = AdmiDatos.verAdmis();
				for (int i = 0; i < admi.size(); i++) {
					if(dni.equalsIgnoreCase(admi.get(i).getdni()) || email.equalsIgnoreCase(admi.get(i).getEmail())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				
				List<Paciente> paciente = PacienteDatos.verPacientes();
				for (int i = 0; i < paciente.size(); i++) {
					if(dni.equalsIgnoreCase(paciente.get(i).getdni()) || email.equalsIgnoreCase(paciente.get(i).getEmail())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				
				List<Medico> medico = MedicoDatos.verMedicos();
				for (int i = 0; i < medico.size(); i++) {
					if(dni.equalsIgnoreCase(medico.get(i).getdni()) || email.equalsIgnoreCase(medico.get(i).getEmail())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				*/
				AdmiDatos.insertar(nombre, apellidop, apellidom, fechanac,
						estado, sexo, dni, direccionp, lugar, telefono, email);

				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/Confirmar.jsp");
				rd.forward(request, response);

			} else if (request.getParameter("action").equals("show")) {
				
				List<Admi> admis = AdmiDatos.verAdmis();
				request.setAttribute("verAdmis", admis);
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/WEB-INF/verAdmis.jsp");
					rd.forward(request, response);
				
			} else if (request.getParameter("action").equals("borrar")) {

				UserService us = UserServiceFactory.getUserService();
				if (us.isUserAdmin() == true) {
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/WEB-INF/borrarAdmi.jsp");
					rd.forward(request, response);
				} else {
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/WEB-INF/Denegado.jsp");
					rd.forward(request, response);

				}
			} else if (request.getParameter("action").equals("red")) {
				UserService us = UserServiceFactory.getUserService();
				if (us.isUserAdmin() == true) {
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/WEB-INF/nuevoAdmi.jsp");
					rd.forward(request, response);
				} else {
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/WEB-INF/Denegado.jsp");
					rd.forward(request, response);

				}
			}
		} catch (Exception e) {
			RequestDispatcher rd = getServletContext()
					.getRequestDispatcher("/WEB-INF/Error.jsp");
			rd.forward(request, response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
