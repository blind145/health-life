package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import epis.unsa.beans.Admi;
import epis.unsa.beans.Especialidad;

@SuppressWarnings("serial")
public class RegistrarEspecialidad extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("action").equals("create")) {

				String nombre = request.getParameter("nombre");
				String turno = request.getParameter("turno");
//				String[] medicose = request.getParameterValues("medicos");
				String tipo = request.getParameter("tipo");
				String medicos = "";

//				for (int i = 0; i < medicose.length; i++) {

//					medicos += medicose[i] + ", ";
//				}
				/*
				List<Especialidad> esp = EspecialidadDatos.verEspecialidades();
				for (int i = 0; i < esp.size(); i++) {
					if(nombre.equalsIgnoreCase(esp.get(i).getNombre())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				*/
				EspecialidadDatos.insertar(nombre, turno, tipo);

				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/Confirmar.jsp");
				rd.forward(request, response);

			} else if (request.getParameter("action").equals("show")) {

				List<Especialidad> especialidades = EspecialidadDatos
						.verEspecialidades();
				request.setAttribute("verEspecialidades", especialidades);
				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/verEspecialidades.jsp");
				rd.forward(request, response);

			} else if (request.getParameter("action").equals("borrar")) {

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Admi> admi = AdmiDatos.verAdmis();
					for (int i = 0; i < admi.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( admi.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/borrarEspecialidad.jsp");
					rd.forward(request, response);
					 }else if(i==admi.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}
				
			} else if (request.getParameter("action").equals("red")) {

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Admi> admi = AdmiDatos.verAdmis();
					for (int i = 0; i < admi.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( admi.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/nuevaEspecialidad.jsp");
					rd.forward(request, response);
					 }else if(i==admi.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}
			}
		} catch (Exception e) {
			RequestDispatcher rd = getServletContext()
					.getRequestDispatcher("/WEB-INF/Error.jsp");
			rd.forward(request, response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
