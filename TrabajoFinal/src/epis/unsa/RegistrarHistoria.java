package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import epis.unsa.beans.Historia;
import epis.unsa.beans.Medico;
import epis.unsa.beans.Paciente;

@SuppressWarnings("serial")
public class RegistrarHistoria extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("action").equals("create")) {

				String numero = request.getParameter("numero");
				String interno = request.getParameter("interno");
				String dni = request.getParameter("dni");
				String fecha = request.getParameter("fecha");
				String talla = request.getParameter("talla");
				String peso = request.getParameter("peso");
				String persona = request.getParameter("persona");
				String relacion = request.getParameter("relacion");
				String medico = request.getParameter("medico");
				String especialidad = request.getParameter("especialidad");
				String her = request.getParameter("her");
				String pat = request.getParameter("pat");
				String[] adicciones = request.getParameterValues("adicciones");
				String descripcion = request.getParameter("descripcion");
				String alimentacion = request.getParameter("alimentacion");
				String deportes = request.getParameter("deportes");
				String inmune = request.getParameter("inmune");
				String alergias = request.getParameter("alergias");
				String padec = request.getParameter("padec");
				String[] sintomas = request.getParameterValues("sintomas");
				String diges = request.getParameter("diges");
				String cardio = request.getParameter("cardio");
				String resp = request.getParameter("resp");
				String ur = request.getParameter("ur");
				String gen = request.getParameter("gen");
				String hema = request.getParameter("hema");
				String endo = request.getParameter("endo");
				String osteo = request.getParameter("osteo");
				String nerv = request.getParameter("nerv");
				String sens = request.getParameter("sens");
				String psico = request.getParameter("psico");
				String anteriores = request.getParameter("anteriores");
				String terapia = request.getParameter("terapia");
				String fc = request.getParameter("fc");
				String fr = request.getParameter("fr");
				String ta = request.getParameter("ta");
				String t = request.getParameter("t");
				String exploracion = request.getParameter("exploracion");
				String cabeza = request.getParameter("cabeza");
				String cuello = request.getParameter("cuello");
				String torax = request.getParameter("torax");
				String abdomen = request.getParameter("abdomen");
				String miembros = request.getParameter("miembros");
				String genes = request.getParameter("genes");
				String comentario = request.getParameter("comentario");
				String diagnostico = request.getParameter("diagnostico");
				String pronostico = request.getParameter("pronostico");
				String tratamiento = request.getParameter("tratamiento");
				String adiccion = "";
				for (int i = 0; i < adicciones.length; i++) {

					adiccion += adicciones[i] + ", ";
				}
				String sintoma = "";
				for (int i = 0; i < sintomas.length; i++) {

					sintoma += sintomas[i] + ", ";
				}

				HistoriaDatos.insertar(numero, interno, dni, fecha, talla,
						peso, persona, relacion, medico, especialidad, her,
						pat, adiccion, descripcion, alimentacion, deportes,
						inmune, alergias, padec, sintoma, diges, cardio, resp,
						ur, gen, hema, endo, osteo, nerv, sens, psico,
						anteriores, terapia, fc, fr, ta, t, exploracion,
						cabeza, cuello, torax, abdomen, miembros, genes,
						comentario, diagnostico, pronostico, tratamiento);

				List<Paciente> pacientes = PacienteDatos.verPacientes();

				String nombrep = "";
				String apellidop = "";
				String apellidom = "";
				for (int i = 0; i < pacientes.size(); i++) {
					Paciente paciente = (Paciente) pacientes.get(i);

					if (dni.equals(paciente.getdni())) {
						nombrep = paciente.getNombre();
						apellidop = paciente.getApellidop();
						apellidom = paciente.getApellidom();
						break;
					}
				}


				request.setAttribute("numero", numero);
				request.setAttribute("nombrep", nombrep);
				request.setAttribute("apellidopp", apellidop);
				request.setAttribute("apellidomp", apellidom);
				request.setAttribute("medico", medico);
				

				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/receta.jsp");
				rd.forward(request, response);

			} else if (request.getParameter("action").equals("show")) {

				
						List<Historia> historias = HistoriaDatos.verHistorias();
						request.setAttribute("verHistorias", historias);
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/verHistorias.jsp");
					rd.forward(request, response);
					

			} else if (request.getParameter("action").equals("red")) {

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Medico> medico = MedicoDatos.verMedicos();
					for (int i = 0; i < medico.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( medico.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/nuevaHistoria.jsp");
					rd.forward(request, response);
					 }else if(i==medico.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}

			} else if (request.getParameter("action").equals("buscar")) {

				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/buscarHistoria.jsp");
				rd.forward(request, response);

			}

		} catch (Exception e) {

			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/WEB-INF/Error.jsp");
			rd.forward(request, response);

		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
