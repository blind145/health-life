package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import epis.unsa.beans.Admi;
import epis.unsa.beans.Medico;

@SuppressWarnings("serial")
public class RegistrarMedico extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("action").equals("create")) {

				String nombre = request.getParameter("nombre");
				String apellidop = request.getParameter("apellidop");
				String apellidom = request.getParameter("apellidom");
				String dia = request.getParameter("dia");
				String mes = request.getParameter("mes");
				String anio = request.getParameter("anio");
				String fechanac = dia + "/" + mes + "/" + anio;
				String estado = request.getParameter("estado");
				String dni = request.getParameter("dni");
				String sexo = request.getParameter("sexo");
				String especialidad = request.getParameter("especialidad");
				String cmp = request.getParameter("cmp");
				String direccion = request.getParameter("direccion");
				String numero = request.getParameter("numero");
				String distrito = request.getParameter("distrito");
				String provincia = request.getParameter("provincia");
				String departamento = request.getParameter("departamento");
				String direccionm = direccion + " - " + numero + " / "
						+ distrito + " / " + provincia + " / " + departamento;
				String lugar = request.getParameter("lugar");
				String email = request.getParameter("email");
				String telefono = request.getParameter("telefono");

				/*
				List<Admi> admi = AdmiDatos.verAdmis();
				for (int i = 0; i < admi.size(); i++) {
					if(dni.equalsIgnoreCase(admi.get(i).getdni()) || email.equalsIgnoreCase(admi.get(i).getEmail())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				
				List<Paciente> paciente = PacienteDatos.verPacientes();
				for (int i = 0; i < paciente.size(); i++) {
					if(dni.equalsIgnoreCase(paciente.get(i).getdni()) || email.equalsIgnoreCase(paciente.get(i).getEmail())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				
				List<Medico> medico = MedicoDatos.verMedicos();
				for (int i = 0; i < medico.size(); i++) {
					if(dni.equalsIgnoreCase(medico.get(i).getdni()) || email.equalsIgnoreCase(medico.get(i).getEmail())){
						RequestDispatcher rd = getServletContext()
								.getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				*/
				
				MedicoDatos.insertar(nombre, apellidop, apellidom, fechanac,
						estado, dni, sexo, especialidad, cmp, direccionm,
						lugar, email, telefono);

				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/Confirmar.jsp");
				rd.forward(request, response);

			} else if (request.getParameter("action").equals("show")) {

				List<Medico> medicos = MedicoDatos.verMedicos();
				request.setAttribute("verMedicos", medicos);
				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/verMedicos.jsp");
				rd.forward(request, response);
				
			} else if (request.getParameter("action").equals("borrar")) {

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Admi> admi = AdmiDatos.verAdmis();
					for (int i = 0; i < admi.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( admi.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/borrarMedico.jsp");
					rd.forward(request, response);
					 }else if(i==admi.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}

			} else if (request.getParameter("action").equals("red")) {

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Admi> admi = AdmiDatos.verAdmis();
					for (int i = 0; i < admi.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( admi.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/nuevoMedico.jsp");
					rd.forward(request, response);
					 }else if(i==admi.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}
			}
		} catch (Exception e) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/WEB-INF/Error1.jsp");
			rd.forward(request, response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
