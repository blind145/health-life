package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import epis.unsa.beans.Admi;
import epis.unsa.beans.Paciente;

@SuppressWarnings("serial")
public class RegistrarPaciente extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("action").equals("create")) {
				
				String nombre = request.getParameter("nombre");
				String apellidop = request.getParameter("apellidop");
				String apellidom = request.getParameter("apellidom");
				String dia = request.getParameter("dia");
				String mes = request.getParameter("mes");
				String anio = request.getParameter("anio");
				String fechanac = dia + "/" + mes + "/" + anio;
				String estado = request.getParameter("estado");
				String grupo = request.getParameter("grupo");
				String grupoSang = request.getParameter("grupoSang");
				String sexo = request.getParameter("sexo");
				String ocupacion = request.getParameter("ocupacion");
				String dni = request.getParameter("dni");
				String direccion = request.getParameter("direccion");
				String numero = request.getParameter("numero");
				String distrito = request.getParameter("distrito");
				String provincia = request.getParameter("provincia");
				String departamento = request.getParameter("departamento");
				String direccionp = direccion + " - " + numero + " / "
						+ distrito + " / " + provincia + " / "
						+ departamento;
				String lugar = request.getParameter("lugar");
				String telefono = request.getParameter("telefono");
				String email = request.getParameter("email");
				String a;
				if(grupoSang.equalsIgnoreCase("1")){
					a="+";
				}else{
					a="-";
				}
				String grupop = grupo+a;
				
				/*
				List<Admi> admi = AdmiDatos.verAdmis();
				for (int i = 0; i < admi.size(); i++) {
					if(dni.equalsIgnoreCase(admi.get(i).getdni()) || email.equalsIgnoreCase(admi.get(i).getEmail())){
						System.out.println("admi");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				
				List<Paciente> paciente = PacienteDatos.verPacientes();
				for (int i = 0; i < paciente.size(); i++) {
					if(dni.equalsIgnoreCase(paciente.get(i).getdni()) || email.equalsIgnoreCase(paciente.get(i).getEmail())){
						System.out.println("paciente");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				
				List<Medico> medico = MedicoDatos.verMedicos();
				for (int i = 0; i < medico.size(); i++) {
					if(dni.equalsIgnoreCase(medico.get(i).getdni()) || email.equalsIgnoreCase(medico.get(i).getEmail())){
						System.out.println("entro");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Error1.jsp");
						rd.forward(request, response);
					}
				}
				*/
				
				PacienteDatos.insertar(nombre, apellidop, apellidom,
						fechanac, estado, grupop, sexo, ocupacion, dni,
						direccionp, lugar, telefono, email);
				
				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/Confirmar.jsp");
				rd.forward(request, response);
				

			} else if (request.getParameter("action").equals("show")) {
				
				List<Paciente> pacientes = PacienteDatos.verPacientes();
				request.setAttribute("verPacientes", pacientes);
				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/verPacientes.jsp");
				rd.forward(request, response);
				
			}
			else if(request.getParameter("action").equals("borrar")){

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Admi> admi = AdmiDatos.verAdmis();
					for (int i = 0; i < admi.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( admi.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/borrarPaciente.jsp");
					rd.forward(request, response);
					 }else if(i==admi.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}
				
			}
			else if(request.getParameter("action").equals("red")){

				 UserService us = UserServiceFactory.getUserService();
					User user = us.getCurrentUser();
					List<Admi> admi = AdmiDatos.verAdmis();
					for (int i = 0; i < admi.size(); i++) {
					if(user.getEmail().equalsIgnoreCase( admi.get(i).getEmail())){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/nuevoPaciente.jsp");
					rd.forward(request, response);
					 }else if(i==admi.size()-1){
						 
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Denegado.jsp");
							rd.forward(request, response);
					 }
					}
			}
		}
			catch (Exception e) {
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/Error1.jsp");
				rd.forward(request, response);
			}
		
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
