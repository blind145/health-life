package epis.unsa;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

@SuppressWarnings("serial")
public class RegistrarReceta extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
	
		TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
		SimpleDateFormat sdf = new SimpleDateFormat();
		
		try {
			
			if (request.getParameter("action").equals("receta")) {

				String numero = request.getParameter("numero");
				String numeroc = request.getParameter("numeroc");
				String paciente = request.getParameter("paciente");
				String fecha = request.getParameter("fecha");
				String rp = request.getParameter("rp");
				String indic = request.getParameter("indic");
				String medico = request.getParameter("medico");

				RecetaDatos.insertar(numero, numeroc, paciente, fecha, rp, indic, medico);

				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("/WEB-INF/Confirmar.jsp");
				rd.forward(request, response);

			}
			
		} catch (Exception e) {
			RequestDispatcher rd = getServletContext()
					.getRequestDispatcher("/WEB-INF/Error.jsp");
			rd.forward(request, response);
		}
			
		}
	

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
