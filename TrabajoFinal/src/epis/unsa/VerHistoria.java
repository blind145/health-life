package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.RequestDispatcher;

import epis.unsa.beans.Historia;
import epis.unsa.beans.Paciente;

@SuppressWarnings("serial")
public class VerHistoria extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
		SimpleDateFormat sdf = new SimpleDateFormat();
		try {
			List<Historia> historias = HistoriaDatos.verHistorias();
			
			String numero = "";
			String interno = "";
			String dni = "";
			String fecha = "";
			String talla = "";
			String peso = "";
			String persona = "";
			String relacion = "";
			String medico = "";
			String especialidad = "";
			String her = "";
			String pat = "";
			String adiccion = "";
			String descripcion = "";
			String alimentacion = "";
			String deportes = "";
			String inmune = "";
			String alergias = "";
			String padec = "";
			String sintoma = "";
			String diges = "";
			String cardio = "";
			String resp = "";
			String ur = "";
			String gen = "";
			String hema = "";
			String endo = "";
			String osteo = "";
			String nerv = "";
			String sens = "";
			String psico = "";
			String anteriores = "";
			String terapia = "";
			String fc = "";
			String fr = "";
			String ta = "";
			String t = "";
			String exploracion = "";
			String cabeza = "";
			String cuello = "";
			String torax = "";
			String abdomen = "";
			String miembros = "";
			String genes = "";
			String comentario = "";
			String diagnostico = "";
			String pronostico = "";
			String tratamiento = "";

			for (int i = 0; i < historias.size(); i++) {
				Historia historia = (Historia) historias.get(i);

				if (request.getParameter("action").equals(historia.getNumero())) {

					numero = historia.getNumero();
					interno = historia.getInterno();
					dni = historia.getDNI();
					fecha = sdf.format(historia.getFecha());
					talla = historia.getTalla();
					peso = historia.getPeso();
					persona = historia.getPersona();
					relacion = historia.getRelacion();
					medico = historia.getMedico();
					especialidad = historia.getEspecialidad();
					her = historia.getHer();
					pat = historia.getPat();
					adiccion = historia.getAdiccion();
					descripcion = historia.getDescripcion();
					alimentacion = historia.getAlimentacion();
					deportes = historia.getDeportes();
					inmune = historia.getInmune();
					alergias = historia.getAlergias();
					padec = historia.getPadec();
					sintoma = historia.getSintoma();
					diges = historia.getDiges();
					cardio = historia.getCardio();
					resp = historia.getResp();
					ur = historia.getUr();
					gen = historia.getGen();
					hema = historia.getHema();
					endo = historia.getEndo();
					osteo = historia.getOsteo();
					nerv = historia.getNerv();
					sens = historia.getSens();
					psico = historia.getPsico();
					anteriores = historia.getAnteriores();
					terapia = historia.getTerapia();
					fc = historia.getFc();
					fr = historia.getFr();
					ta = historia.getTa();
					t = historia.getT();
					exploracion = historia.getExploracion();
					cabeza = historia.getCabeza();
					cuello = historia.getCuello();
					torax = historia.getTorax();
					abdomen = historia.getAbdomen();
					miembros = historia.getMiembros();
					genes = historia.getGenes();
					comentario = historia.getComentario();
					diagnostico = historia.getDiagnostico();
					pronostico = historia.getPronostico();
					tratamiento = historia.getTratamiento();
					break;
				}
			}
			
			List<Paciente> pacientes = PacienteDatos.verPacientes();
		
			String nombre="";
			String apellidop="";
			String apellidom="";
			String fechanac="";
			String estado="";
			String ocupacion="";
			String grupo="";
			String sexo="";
			String direccion="";
			String lugar="";
			
			for (int i = 0; i < pacientes.size(); i++) {
				Paciente paciente = (Paciente) pacientes.get(i);

				if (dni.equals(paciente.getdni())) {
					nombre = paciente.getNombre();
					apellidop= paciente.getApellidop();
					apellidom= paciente.getApellidom();
					fechanac=paciente.getFechanac();
					estado=paciente.getEstado();
					ocupacion=paciente.getOcupacion();
					grupo= paciente.getGrupo();
					sexo=paciente.getSexo();
					direccion = paciente.getDireccion();
					lugar = paciente.getLugar();
					break;
				}
			}


			request.setAttribute("historias", historias);
			request.setAttribute("numero", numero);
			request.setAttribute("interno", interno);
			request.setAttribute("nombre", nombre);
			request.setAttribute("apellidop", apellidop);
			request.setAttribute("apellidom", apellidom);
			request.setAttribute("fechanac", fechanac);
			request.setAttribute("estado", estado);
			request.setAttribute("ocupacion", ocupacion);
			request.setAttribute("grupo", grupo);
			request.setAttribute("sexo", sexo);
			request.setAttribute("direccion", direccion);
			request.setAttribute("lugar", lugar);
			request.setAttribute("dni", dni);
			request.setAttribute("fecha", fecha);
			request.setAttribute("talla", talla);
			request.setAttribute("peso", peso);
			request.setAttribute("persona", persona);
			request.setAttribute("relacion", relacion);
			request.setAttribute("medico", medico);
			request.setAttribute("especialidad", especialidad);
			request.setAttribute("her", her);
			request.setAttribute("pat", pat);
			request.setAttribute("adiccion", adiccion);
			request.setAttribute("descripcion", descripcion);
			request.setAttribute("alimentacion", alimentacion);
			request.setAttribute("deportes", deportes);
			request.setAttribute("inmune", inmune);
			request.setAttribute("alergias", alergias);
			request.setAttribute("padec", padec);
			request.setAttribute("sintoma", sintoma);
			request.setAttribute("diges", diges);
			request.setAttribute("cardio", cardio);
			request.setAttribute("resp", resp);
			request.setAttribute("ur", ur);
			request.setAttribute("gen", gen);
			request.setAttribute("hema", hema);
			request.setAttribute("endo", endo);
			request.setAttribute("osteo", osteo);
			request.setAttribute("nerv", nerv);
			request.setAttribute("sens", sens);
			request.setAttribute("psico", psico);
			request.setAttribute("anteriores", anteriores);
			request.setAttribute("terapia", terapia);
			request.setAttribute("fc", fc);
			request.setAttribute("fr", fr);
			request.setAttribute("ta", ta);
			request.setAttribute("t", t);
			request.setAttribute("exploracion", exploracion);
			request.setAttribute("cabeza", cabeza);
			request.setAttribute("cuello", cuello);
			request.setAttribute("torax", torax);
			request.setAttribute("abdomen", abdomen);
			request.setAttribute("miembros", miembros);
			request.setAttribute("genes", genes);
			request.setAttribute("comentario", comentario);
			request.setAttribute("diagnostico", diagnostico);
			request.setAttribute("pronostico", pronostico);
			request.setAttribute("tratamiento", tratamiento);
			
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/WEB-INF/verHistoria.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/WEB-INF/Error.jsp");
			rd.forward(request, response);

		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
