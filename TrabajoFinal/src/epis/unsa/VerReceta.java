package epis.unsa;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.RequestDispatcher;

import epis.unsa.beans.Receta;

@SuppressWarnings("serial")
public class VerReceta extends HttpServlet {

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
		SimpleDateFormat sdf = new SimpleDateFormat();
		try {
			List<Receta> receta = RecetaDatos.verReceta();
			
			String numero = "";
			String numeroc = "";
			String paciente = "";
			String fecha = "";
			String rp = "";
			String indic = "";
			String medico = "";
			

			for (int i = 0; i < receta.size(); i++) {
				Receta mireceta = (Receta) receta.get(i);

				if (request.getParameter("action").equals(mireceta.getNumero())) {

					numero = mireceta.getNumero();
					numeroc = mireceta.getNumeroc();
					paciente = mireceta.getPaciente();
					fecha = sdf.format(mireceta.getFecha());
					rp = mireceta.getRp();
					indic = mireceta.getIndic();
					medico = mireceta.getMedico();
					System.out.println("dfgh");
					break;
				}
			}		
			
			request.setAttribute("numero", numero);
			request.setAttribute("numeroc", numeroc);
			request.setAttribute("paciente", paciente);
			request.setAttribute("fecha", fecha);
			request.setAttribute("rp", rp);
			request.setAttribute("indic", indic);
			request.setAttribute("medico", medico);
			
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/WEB-INF/verReceta.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/WEB-INF/Error.jsp");
			rd.forward(request, response);

		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
