package epis.unsa.beans;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.jdo.annotations.*;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@PersistenceCapable (identityType=IdentityType.APPLICATION)
public class Admi {
	
	 @PrimaryKey
	
	 @Persistent(valueStrategy=IdGeneratorStrategy.SEQUENCE)
	 private Long id;
	 @Persistent 
	 private String nombre;
	 @Persistent 
	 private String apellidop;
	 @Persistent 
	 private String apellidom;
	 @Persistent
	 private String fechanac;
	 @Persistent
	 private String estado;
	 @Persistent
	 private String sexo;
	 @Persistent
	 private String dni;
	 @Persistent
	 private String direccion;
	 @Persistent
	 private String lugar;
	 @Persistent
	 private String telefono;
	 @Persistent
	 private String email;
	 @Persistent
	 private Date fecha;
	
	 public Admi(String nombre, String apellidop, String apellidom, String fechanac, String estado,
			 String sexo, String dni, String direccion,
			 String lugar, String telefono, String email){
	  this.nombre = nombre;
	  this.apellidop = apellidop;
	  this.apellidom = apellidom;
	  this.fechanac = fechanac;
	  this.estado = estado;
	  this.sexo = sexo;
	  this.dni = dni;
	  this.direccion = direccion;
	  this.lugar = lugar;
	  this.telefono = telefono;
	  this.email = email;

	  TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
	  this.fecha = new Date();
	 }
	
	 public String getNombre() {
	  return nombre;
	 }
	 
	 public String getApellidop() {
		  return apellidop;
		 }
	 public String getApellidom() {
		  return apellidom;
		 }
	 
	 public String getFechanac() {
		  return fechanac;
		 }
	 
	 public String getEstado() {
		  return estado;
		 }
	 
	 public String getSexo() {
		  return sexo;
		 }
	 
	 public String getdni() {
		  return dni;
		 }
	 
	 public String getDireccion() {
		  return direccion;
		 }
	 public String getLugar() {
		  return lugar;
		 }
	 public String getTelefono() {
		  return telefono;
		 }
	 public String getEmail() {
		  return email;
		 }
	 public Date getFecha() {
		  return fecha;
		 }
	 public Long getId() {
	  return id;
	 }
	 public void setId(Long id) {
	  this.id = id;
	 }

}
