package epis.unsa.beans;

import java.util.Date;
import java.util.TimeZone;
import javax.jdo.annotations.*;

@PersistenceCapable (identityType=IdentityType.APPLICATION)
public class Especialidad{
	
	 @PrimaryKey
	 @Persistent(valueStrategy=IdGeneratorStrategy.SEQUENCE)
	 private Long id;
	 @Persistent 
	 private String nombre;
	 @Persistent 
	 private String turno;
	 @Persistent
	 private String tipo;
	 @Persistent
	 private Date fecha;
	
	
	 public Especialidad(String nombre, String turno, String tipo){
		 
		 this.nombre = nombre;
		 this.turno =turno;
		 this.tipo = tipo;	

	  TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
	  this.fecha = new Date();
	 }
	
	 public String getNombre() {
	  return nombre;
	 }
	 
	 public String getTurno() {
		  return turno;
		 }
	 public String getTipo() {
		  return tipo;
		 }
	 public Date getFecha() {
		  return fecha;
		 }
	 public Long getId() {
	  return id;
	 }
	 public void setId(Long id) {
	  this.id = id;
	 }
}
