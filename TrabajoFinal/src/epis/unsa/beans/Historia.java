package epis.unsa.beans;

import java.util.Date;
import java.util.TimeZone;
import javax.jdo.annotations.*;

@PersistenceCapable (identityType=IdentityType.APPLICATION)
public class Historia{
	
	 @PrimaryKey
	 @Persistent(valueStrategy=IdGeneratorStrategy.SEQUENCE)
	 private Long id;
	 @Persistent 
	 private String numero;
	 @Persistent 
	 private String interno;
	 @Persistent 
	 private String dni;
	 @Persistent
	 private Date fecha;
	 @Persistent
	 private String talla;
	 @Persistent
	 private String peso;
	 @Persistent
	 private String persona;
	 @Persistent
	 private String relacion;
	 @Persistent
	 private String medico;
	 @Persistent
	 private String especialidad;
	 @Persistent
	 private String her;
	 @Persistent
	 private String pat;
	 @Persistent
	 private String adiccion;
	 @Persistent
	 private String descripcion;
	 @Persistent
	 private String alimentacion;
	 @Persistent
	 private String deportes;
	 @Persistent
	 private String inmune;
	 @Persistent
	 private String alergias;
	 @Persistent
	 private String padec;
	 @Persistent
	 private String sintoma;
	 @Persistent
	 private String diges;
	 @Persistent
	 private String cardio;
	 @Persistent
	 private String resp;
	 @Persistent
	 private String ur;
	 @Persistent
	 private String gen;
	 @Persistent
	 private String hema;
	 @Persistent
	 private String endo;
	 @Persistent
	 private String osteo;
	 @Persistent
	 private String nerv;
	 @Persistent
	 private String sens;
	 @Persistent
	 private String psico;
	 @Persistent
	 private String anteriores;
	 @Persistent
	 private String terapia;
	 @Persistent
	 private String fc;
	 @Persistent
	 private String fr;
	 @Persistent
	 private String ta;
	 @Persistent
	 private String t;
	 @Persistent
	 private String exploracion;
	 @Persistent
	 private String cabeza;
	 @Persistent
	 private String cuello;
	 @Persistent
	 private String torax;
	 @Persistent
	 private String abdomen;
	 @Persistent
	 private String miembros;
	 @Persistent
	 private String genes;
	 @Persistent
	 private String comentario;
	 @Persistent
	 private String diagnostico;
	 @Persistent
	 private String pronostico;
	 @Persistent
	 private String tratamiento;
	
	 public Historia(String numero, String interno,String dni,String fecha,String talla,String peso,String persona,
				String relacion,String medico,String especialidad, String her,String pat,String adiccion,String descripcion, String alimentacion,
				String deportes,String inmune,String alergias,String padec,String sintoma,String diges,String cardio,String resp,
				String ur,String gen,String hema,String endo,String osteo,String nerv,String sens,String psico,String anteriores,
				String terapia,String fc,String fr,String ta,String t,String exploracion,String cabeza,String cuello,String torax,
				String abdomen,String miembros,String genes,String comentario,String diagnostico,String pronostico,String tratamiento){
		 
		 this.numero= numero;	
		 this.interno= interno;	
		 this.dni= dni;
		 TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
		 this.fecha = new Date();
		 this.talla = talla;	
		 this.peso= peso;	
		 this.persona= persona;	
		 this.relacion= relacion;
		 this.medico= medico;	
		 this.especialidad= especialidad;	
		 this.her= her;	
		 this.pat= pat;	
		 this.adiccion= adiccion;
		 this.descripcion= descripcion;	
		 this.alimentacion= alimentacion;	
		 this.deportes= deportes;	
		 this.inmune= inmune;	
		 this.alergias= alergias;	
		 this.padec= padec;	
		 this.sintoma= sintoma;	
		 this.diges= diges;	
		 this.cardio= cardio;	
		 this.resp= resp;	
		 this.ur= ur;	
		 this.gen= gen;	
		 this.hema= hema;	
		 this.endo= endo;	
		 this.osteo= osteo;	
		 this.nerv= nerv;	
		 this.sens= sens;
		 this.psico= psico;	
		 this.anteriores= anteriores;	
		 this.terapia= terapia;	
		 this.fc= fc;	
		 this.fr= fr;	
		 this.ta= ta;	
		 this.t= t;	
		 this.exploracion= exploracion;	
		 this.cabeza= cabeza;	
		 this.cuello= cuello;
		 this.torax= torax;	
		 this.abdomen= abdomen;	
		 this.miembros= miembros;	
		 this.genes= genes;	
		 this.comentario= comentario;	
		 this.diagnostico= diagnostico;	
		 this.pronostico= pronostico;	
		 this.tratamiento= tratamiento;	
	 }
	
	 public String getNumero() {
	  return numero;
	 }
	 
	 public String getInterno() {
		  return interno;
		 }
	 public String getDNI() {
		  return dni;
		 }
	 public Date getFecha() {
		  return fecha;
		 }
	 public String getTalla() {
		  return talla;
		 }
	 public String getPeso() {
		  return peso;
		 }
	 public String getPersona() {
		  return persona;
		 }
	 public String getRelacion() {
		  return relacion;
		 }
	 public String getMedico() {
		  return medico;
		 }
	 public String getEspecialidad() {
		  return especialidad;
		 }
	 public String getHer() {
		  return her;
		 }
	 public String getPat() {
		  return pat;
		 }
	 public String getAdiccion() {
		  return adiccion;
		 }
	 public String getDescripcion() {
		  return descripcion;
		 }
	 public String getAlimentacion() {
		  return alimentacion;
		 }
	 public String getDeportes() {
		  return deportes;
		 }
	 public String getInmune() {
		  return inmune;
		 }
	 public String getAlergias() {
		  return alergias;
		 }
	 public String getPadec() {
		  return padec;
		 }
	 public String getSintoma() {
		  return sintoma;
		 }
	 public String getDiges() {
		  return diges;
		 }
	 public String getCardio() {
		  return cardio;
		 }
	 public String getResp() {
		  return resp;
		 }
	 public String getUr() {
		  return ur;
		 }
	 public String getGen() {
		  return gen;
		 }
	 
	 public String getHema() {
		  return hema;
		 }
	 public String getEndo() {
		  return endo;
		 }
	 public String getOsteo() {
		  return osteo;
		 }
	 public String getNerv() {
		  return nerv;
		 }
	 public String getSens() {
		  return sens;
		 }
	 public String getPsico() {
		  return psico;
		 }
	 public String getAnteriores() {
		  return anteriores;
		 }
	 public String getTerapia() {
		  return terapia;
		 }
	 public String getFc() {
		  return fc;
		 }
	 public String getFr() {
		  return fr;
		 }
	 public String getTa() {
		  return ta;
		 }
	 public String getT() {
		  return t;
		 }
	 public String getExploracion() {
		  return exploracion;
		 }
	 public String getCabeza() {
		  return cabeza;
		 }
	 public String getCuello() {
		  return cuello;
		 }
	 public String getTorax() {
		  return torax;
		 }
	 public String getAbdomen() {
		  return abdomen;
		 }
	 public String getMiembros() {
		  return miembros;
		 }
	 public String getGenes() {
		  return genes;
		 }
	 public String getComentario() {
		  return comentario;
		 }
	 public String getDiagnostico() {
		  return diagnostico;
		 }
	 public String getPronostico() {
		  return pronostico;
		 }
	 public String getTratamiento() {
		  return tratamiento;
		 }
	 
}
