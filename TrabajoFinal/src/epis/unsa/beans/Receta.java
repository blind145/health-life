package epis.unsa.beans;

import java.util.Date;
import java.util.TimeZone;
import javax.jdo.annotations.*;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Receta {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.SEQUENCE)
	private Long id;
	@Persistent
	private String numero;
	@Persistent
	private String numeroc;
	@Persistent
	private String paciente;
	@Persistent
	private String rp;
	@Persistent
	private String indic;
	@Persistent
	private String medico;
	@Persistent
	private Date fecha;

	public Receta(String numero, String numeroc, String paciente, String fecha,
			String rp, String indic, String medico) {

		this.numero = numero;
		this.numeroc = numeroc;
		this.paciente = paciente;
		this.rp = rp;
		this.indic = indic;
		this.medico = medico;

		TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
		this.fecha = new Date();
	}

	public String getNumero() {
		return numero;
	}

	public String getNumeroc() {
		return numeroc;
	}

	public String getPaciente() {
		return paciente;
	}

	public String getRp() {
		return rp;
	}
	public String getIndic() {
		return indic;
	}
	public String getMedico() {
		return medico;
	}
	public Date getFecha() {
		return fecha;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
