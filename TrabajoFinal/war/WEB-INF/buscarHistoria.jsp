<%@page import="epis.unsa.*" %>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat" %>

<%@page import="epis.unsa.beans.*" %>
<% List<Medico> medicos = MedicoDatos.verMedicos();%>
<% List<Especialidad> especialidades = EspecialidadDatos.verEspecialidades();%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>

	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>

	<div class="body">
		<h2>Buscar Historias Clinicas</h2>
		<hr>
		<form action="/buscarHistorias" method="post">

			<fieldset>
				<legend>1) POR DNI</legend>
				<table>
					<tr>
						<td>Ingrese numero de DNI:</td>
						<td><input type="text" name="dni" maxlength="8" 
							pattern="[0-9]{8}" title="Ingrese solo numeros">
						<td> <input
							type="submit" name="action" value="Busqueda por DNI"></td>
					</tr>
				</table>
			</fieldset>

			<br>

			<fieldset>
				<legend>2) POR DOCTOR</legend>

				<table>
					<tr>
						<td>Seleccione Medico:</td>
						<td><select name="medico">
								<option>          </option>
								<%for (int i = 0; i < medicos.size(); i++) {
    Medico medico = (Medico)medicos.get(i);%>
								<option value="Dr.(a) <%= medico.getNombre() %> <%= medico.getApellidop() %>">
								Dr.(a) <%= medico.getNombre() %> <%= medico.getApellidop() %></option>
								<%}%>
						</select></td>
						<td>
						<input type="submit" name="action" value="Busqueda por medico"></td>
					</tr>
				</table>
			</fieldset>

			<br>

			<fieldset>
				<legend>2) POR ESPECIALIDAD</legend>

				<table>
					<tr>
						<td>Seleccione Especialidad:</td>
						<td><select name= "especialidad">
								<option>              </option>
								<%for (int i = 0; i < especialidades.size(); i++) {
    Especialidad especialidad = (Especialidad)especialidades.get(i);%>
								<option value="<%= especialidad.getNombre() %>"><%= especialidad.getNombre() %></option>
								<%}%>
						</select></td>
						<td>
						<input type="submit" name="action" value="Busqueda por especialidad"></td>
					</tr>
				</table>
			</fieldset>

			<br>

		</form>
		

		<address>Policlinico Health Life</address>
		   <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" />
    </a>
</p>

<p>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valid CSS!" />
    </a>
	</div>
</body>
</html>