<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" alt="logo" src="/logo.png"></a>
			</th>

			<th class="titulo">
				SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS
			</th>
			<th><a href="cerrar"> Cerrar sesion </a></th>
		</tr>
	</table>
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	<div class="body">
		<h2>Bienvenido</h2>
		<hr>
		<p>El sistema de historias clinicas es un registro unificado y
			personal, en el que se archiva toda la informacion referente al
			paciente y a su atencion.</p>
		<p>Es accesible, con las limitaciones apropiadas, en todos los
			casos en los que se precisa asistencia clinica.</p>
		
			<div class = "inicio"> <img src="h1.jpg" alt="h1" height="200" width="300" /> <img src="h2.jpg"
				alt="h2" height="200" width="300" /></div>
				
		<a href="https://docs.google.com/document/d/1Zn0FicmIgcNAVpx7BTtrtJpLY-2MOXCfOJx-9ygwfas/pub">PLAN DE PROYECTO</a>
		<br>
		<br>
		<a href="https://docs.google.com/document/d/1JVE4fy1DWYmleMPuZ5Nt7TTjvnogGP7jESeeeFZ5RGc/pub">CASOS DE PRUEBA</a>
		

	<address>Policlinico Health Life
<p>
    <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" />
    </a>
</p>

<p>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valid CSS!" />
    </a>
</p>
	</address>
	</div>
</body>
</html>
