<%@page import="epis.unsa.*" %>
<%@page import="java.util.*"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="epis.unsa.beans.*" %>
<% List<Medico> medicos = MedicoDatos.verMedicos();%>
<%
	TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String date = sdf.format(new Date());
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>
	
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	
	<div class="body">
		<h2>Registro para crear una nueva especialidad</h2>
		<hr>
		<form action="/registrarEspecialidad" method="post">
			<table>
				<tr>
					<td>Especialidad:</td>
					<td><input type="text" name="nombre" required pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)" title="Ingrese solo letras"></td>
				</tr>
				<tr>
					<td>Turnos de atencion:</td>
					<td><input required type="radio" name="turno" value= "Maniana">Maniana
						<input type="radio" name="turno" value = "Tarde">Tarde 
						<input type="radio" name="turno" value = "Ambos">Ambos 
					</td>
				</tr>
				
				<% //<tr>
					//<td>Medicos encargados:</td>
						//<td>
							//	<ul>
								//<%for (int i = 0; i < medicos.size(); i++) {
    //Medico medico = (Medico)medicos.get(i); % >
		//						<li><input type="checkbox" name= "medicos" value="Dr.(a) <%= medico.getNombre() % > <%= medico.getApellidop() % >">Dr.(a) <%= medico.getNombre() % > < %= medico.getApellidop() % ></li>
			//					< %} % >
				//				</ul>
					//	</td>
			//	</tr>%>
				
				<tr>
					<td>Tipo de especialidad:</td>
					<td><select required name= "tipo">
							<option>         </option>
							<option value="Clinica">Clinica</option>
							<option value="Quirurgica">Quirurgica</option>
							<option value="Medico-Quirurgica">Medico-Quirurgica</option>
							<option value="Laboratorio">Laboratorio</option>
					</select></td>
				</tr>
				
				<tr>
					<td>Fecha de creacion:</td>
					<td><input readonly type="text" name="fecha"
						value="<%=date%>" /></td>
				</tr>

			</table>
			<br> <input type="hidden" name="action" value="create" /> <input
				type="reset" value="Limpiar Registro"> <input type="submit"
				value="Guardar Registro"> <br>

		</form>

		<address>Policlinico Health Life</address>
		   <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" />
    </a>
</p>

<p>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valid CSS!" />
    </a>
	</div>
</body>
</html>