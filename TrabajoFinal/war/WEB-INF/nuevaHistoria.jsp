<%@page import="epis.unsa.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.security.SecureRandom"%>
<%@page import="epis.unsa.beans.*" %>
<%
	List<Medico> medicos = MedicoDatos.verMedicos();
%>
<%
	List<Especialidad> especialidades = EspecialidadDatos.verEspecialidades();
%>

<%
	TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String date = sdf.format(new Date());
%>

<%
	SecureRandom secureRand =SecureRandom.getInstance("SHA1PRNG");
int numHistoria= secureRand.nextInt(10000);
Random rd = new Random();
int kg = rd.nextInt(50)+40;
int gg = rd.nextInt(100);
int m = 1;
int cm = rd.nextInt(50)+30;
String peso = Integer.toString(kg) + "." + Integer.toString(gg) + "Kg";
String talla = m + "." + Integer.toString(cm) + "m";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
	$(document).ready(function() {

		$("#a").hide();
		$("#b").hide();
		$("#c").hide();
		$("#d").hide();
		$("#e").hide();
		$("#f").hide();
		$("#g").hide();
		$("#h").hide();
		$("#i").hide();
		$("#j").hide();
		$("#k").hide();

		$("#a1").click(function() {

			$("#a").show();
		});
		$("#b1").click(function() {

			$("#b").show();
		});
		$("#c1").click(function() {

			$("#c").show();
		});
		$("#d1").click(function() {

			$("#d").show();
		});
		$("#e1").click(function() {

			$("#e").show();
		});
		$("#f1").click(function() {

			$("#f").show();
		});
		$("#g1").click(function() {

			$("#g").show();
		});
		$("#h1").click(function() {

			$("#h").show();
		});
		$("#i1").click(function() {

			$("#i").show();
		});
		$("#j1").click(function() {

			$("#j").show();
		});
		$("#k1").click(function() {

			$("#k").show();
		});
			
		$("#a2").click(function() {

			$("#a").hide();
		});
		$("#b2").click(function() {

			$("#b").hide();
		});
		$("#c2").click(function() {

			$("#c").hide();
		});
		$("#d2").click(function() {

			$("#d").hide();
		});
		$("#e2").click(function() {

			$("#e").hide();
		});
		$("#f2").click(function() {

			$("#f").hide();
		});
		$("#g2").click(function() {

			$("#g").hide();
		});
		$("#h2").click(function() {

			$("#h").hide();
		});
		$("#i2").click(function() {

			$("#i").hide();
		});
		$("#j2").click(function() {

			$("#j").hide();
		});
		$("#k2").click(function() {

			$("#k").hide();
		});
	
	});
</script>
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR
							PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR
							PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul></li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER
							ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR
							ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR
							ESPECIALIDADES</a>
				</ul></li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER
							ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR
							ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR
							ADMINISTRADORES</a>
				</ul></li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR
							HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR
							HISTORIA</a>
				</ul></li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	<div class="body">
		<h2>Crear Nueva Historia Clinica</h2>
		<hr>
		<form action="/registrarHistoria" method="post">
			<fieldset>
				<legend>
					1) FICHA DE IDENTIFICACION <button id="a1">Ver</button>
					<button id="a2">Ocultar</button>
				</legend>
				<div id="a">
				<table>

					<tr>
						<td>H. C. Numero:</td>
						<td><input readonly type="text" name="numero" maxlength="5"
							value="<%=numHistoria%>">
						<td>Internado (Hospitalizado) *:</td>
						<td><input required type="radio" name="interno" value="Si">Si
							<input type="radio" name="interno" value="No">No</td>
					</tr>

					<tr>
						<td>DNI del paciente *:</td>
						<td><input type="text" name="dni" maxlength="8" required
							pattern="[0-9]{8}" title="Ingrese solo numeros"></td>
						<td>Fecha de ingreso/consulta:</td>
						<td><input readonly type="text" name="fecha"
							value="<%=date%>" /></td>

					</tr>

					<tr>
						<td>Talla:</td>
						<td><input readonly type="text" name="talla"
							value="<%=talla%>">
						<td>Peso:</td>
						<td><input readonly type="text" name="peso" value="<%=peso%>">
					</tr>

					<tr>
						<td>Persona Responsable:</td>
						<td><input type="text" name="persona"
							pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)"
							title="Ingrese solo letras"></td>
						<td>Relacion con el paciente:</td>
						<td><input type="text" name="relacion"
							pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)"
							title="Ingrese solo letras"></td>
					</tr>

					<tr>
						<td>Medico Responsable *:</td>
						<td><select required name="medico">
								<option></option>
								<%
									for (int i = 0; i < medicos.size(); i++) {
										Medico medico = (Medico) medicos.get(i);
								%>
								<option
									value="Dr.(a) <%=medico.getNombre()%> <%=medico.getApellidop()%>">
									Dr.(a)
									<%=medico.getNombre()%>
									<%=medico.getApellidop()%></option>
								<%
									}
								%>
						</select></td>
						<td>Especialidad *:</td>
						<td><select required name="especialidad">
								<option></option>
								<%
									for (int i = 0; i < especialidades.size(); i++) {
										Especialidad especialidad = (Especialidad) especialidades
												.get(i);
								%>
								<option value="<%=especialidad.getNombre()%>"><%=especialidad.getNombre()%></option>
								<%
									}
								%>
						</select></td>
					</tr>

				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>2) ANTECEDENTES <button id="b1">Ver</button>
					<button id="b2">Ocultar</button>
				</legend>
				<div id="b">
				a) Heredo Familiares
				<table>
					<tr>
						<td>Tuberculosis, Diabetes Mellitus, Hipertension,
							Carcinomas, Cardiopatias, Hepatopatias, Nefropatias,
							Enf.endocrinas, Enf. Mentales, Epilepsia, Asma, Enf.
							Hematologicas.</td>
						<td><textarea rows="7" cols="30" name="her"></textarea></td>
					</tr>
				</table>

				b) Personales Patologicos
				<table>
					<tr>
						<td>Transfusiones, Intervenciones Quirurgicas, Traumatismos
							Enf. Infecciosas de la infancia, Tb , Enf. Venereas, Fiebre
							Tifoidea, neumonias, Enf. Alergicas, Pad. Articulares</td>
						<td><textarea rows="7" cols="30" name="pat"></textarea></td>
					</tr>
				</table>

				c) Personales no patologicos

				<table>
					<tr>
						<td>Adicciones:</td>
						<td><input type="checkbox" name="adicciones" value="Tabaco">Tabaquismo
							<input type="checkbox" name="adicciones" value="Alcohol">Alcoholismo
							<input type="checkbox" name="adicciones" value="Drogas">Toxicomanias
							<input type="checkbox" name="adicciones" value="Otros">Otros
							<input type="text" name="descripcion" size="50"></td>
					</tr>

					<tr>
						<td>Tipo de alimentacion:</td>
						<td><textarea rows="5" cols="30" name="alimentacion"></textarea></td>
						<td>Deportes:</td>
						<td><textarea rows="5" cols="30" name="deportes"></textarea></td>
					</tr>

					<tr>
						<td>Inmunizaciones:</td>
						<td><textarea rows="5" cols="30" name="inmune"></textarea></td>
						<td>Alergias:</td>
						<td><textarea rows="5" cols="30" name="alergias"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>3) PADECIMIENTO ACTUAL (1 PRINCIPIO, 2 EVOLUCION, 3
					ESTADO ACTUAL) <button id="c1">Ver</button>
					<button id="c2">Ocultar</button></legend>
					<div id="c">
				<table>
					<tr>
						<td><textarea rows="5" cols="115" name="padec"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>4) SINTOMAS GENERALES <button id="d1">Ver</button>
					<button id="d2">Ocultar</button></legend>
<div id="d">
				<table>
					<tr>
						<td><input type="checkbox" value="Astenia" name="sintomas">Astenia
							<input type="checkbox" value="Adinamia" name="sintomas">Adinamia
							<input type="checkbox" value="Anorexia" name="sintomas">Anorexia
							<input type="checkbox" value="Fiebre" name="sintomas">Fiebre
							<input type="checkbox" value="Perdida de peso" name="sintomas">Perdida
							de peso</td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>5) INTERROGATORIO POR APARATOS Y SISTEMAS <button id="e1">Ver</button>
					<button id="e2">Ocultar</button></legend>
					<div id="e">
				<table>
					<tr>
						<td><ins>Aparato digestivo.</ins> Nausea, vomito,
							(hematemesis), dolor abd. meteorismo,constipacion, diarrea</td>
						<td><textarea rows="7" cols="30" name="diges"></textarea></td>
					</tr>

					<tr>
						<td><ins>Aparato cardiovascular.</ins> Disnea, tos (seca.
							prod.), hemoptisis, dolor precordial, palpitaciones, cianosis,
							edema y manifestaciones perifericas (acufenos, fosfenos, sincope,
							lipotimia, cefalea,etc)</td>
						<td><textarea rows="7" cols="30" name="cardio"></textarea></td>
					</tr>

					<tr>
						<td><ins>Aparato respiratorio.</ins> Tos, disnea, dolor
							toraxico, hemoptisis, cianosis, vomica, alteraciones de la voz.</td>
						<td><textarea rows="7" cols="30" name="resp"></textarea></td>
					</tr>

					<tr>
						<td><ins>Aparato urinario.</ins> Alteraciones de la miccion
							(poliuria, anuria, polaquiuria,oliguria, nicturia, opsiuria,
							disuria, tenesmo vesical, urgencia, chorro, enuresis,
							incontenincia) caracteres de la orina (volumen, olor, color,
							aspecto) dolor lumbar, edema renal, hipertension arterial</td>
						<td><textarea rows="7" cols="30" name="ur"></textarea></td>
					</tr>

					<tr>
						<td><ins>Aparato genital.</ins> Criptorquidia, fimosis,
							funcion sexual. Sangrado genital, flujo o leucorrea, dolor
							ginecologico, prurito vulvar</td>
						<td><textarea rows="7" cols="30" name="gen"></textarea></td>
					</tr>

					<tr>
						<td><ins>Aparato hematologico.</ins> Datos clinicos de anemia
							(palidez, astenia, adinamia y otros), hemorragias, adenopatias,
							esplenomegalia.</td>
						<td><textarea rows="7" cols="30" name="hema"></textarea></td>
					</tr>

					<tr>
						<td><ins>Sistema endocrino.</ins> Bocio, letargia,
							bradipsiquia (lalia), intol. calor/frio, nerviosismo,
							hiperquinesis, carac. sexuales, galactorrea, amenorrea,
							ginecomastia, obesidad, ruborizacion.</td>
						<td><textarea rows="7" cols="30" name="endo"></textarea></td>
					</tr>

					<tr>
						<td><ins>Sistema osteomuscular.</ins> Ganglios, xeroftalmia,
							xerostomia, fotosensibilidad artralgias/mialgias, Raynaud</td>
						<td><textarea rows="7" cols="30" name="osteo"></textarea></td>
					</tr>

					<tr>
						<td><ins>Sistema nervioso.</ins> cefalea, sincope,
							convulsiones, deficit transitorio, vertigo, confusion y obnub.,
							vigilia/sue�o, paralisis y M, marcha y equilibrio, sensibilidad.</td>
						<td><textarea rows="7" cols="30" name="nerv"></textarea></td>
					</tr>

					<tr>
						<td><ins>Sistema sensorial.</ins> Vision, agudeza, borrosa
							diplopia, fosgenos, dolor ocular, fotofobia, xeroftalmia,
							amaurosis, otalgia, otorrea y otorragia, hipoacusia, tinitus,
							olfaccion, epistaxis, secrecion</td>
						<td><textarea rows="7" cols="30" name="sens"></textarea></td>
					</tr>

					<tr>
						<td><ins>Psicosomatico</ins> Personalidad, ansiedad,
							depresion, afectividad, emotividad, amnesia, voluntad,
							pensamiento, atencion, ideacion suicida, delirios</td>
						<td><textarea rows="7" cols="30" name="psico"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>6) DIAGNOSTICOS ANTERIORES <button id="f1">Ver</button>
					<button id="f2">Ocultar</button></legend>
					<div id="f">
				<table>
					<tr>
						<td><textarea rows="5" cols="115" name="anteriores"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>7) TERAPEUTICA EMPLEADA ANTERIORMENTE <button id="g1">Ver</button>
					<button id="g2">Ocultar</button></legend>
					<div id="g">
				<table>
					<tr>
						<td><textarea rows="5" cols="115" name="terapia"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>8) SIGNOS VITALES <button id="h1">Ver</button>
					<button id="h2">Ocultar</button></legend>
					<div id="h">
				<table>
					<tr>
						<td>Frecuencia Cardiaca (FC):</td>
						<td><input type="text" name="fc"
							value="<%=(int) Math.random() * 10 + 40%>"></td>
						<td>Frecuencia Respiratoria (FR):</td>
						<td><input type="text" name="fr"
							value="<%=(int) Math.random() * 10 + 30%>"></td>
					</tr>

					<tr>
						<td>Tension Arterial (TA):</td>
						<td><input type="text" name="ta"
							value="<%=(int) Math.random() * 15 + 20%>"></td>
						<td>Temperatura (T):</td>
						<td><input type="text" name="t"
							value="<%=(int) Math.random() * 10 + 35%>"></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>9) EXPLORACION GENERAL <button id="i1">Ver</button>
					<button id="i2">Ocultar</button></legend>
					<div id="i">
				<table>
					<tr>
						<td><textarea rows="5" cols="115" name="exploracion"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>10) EXPLORACION REGIONAL (INSPECCION, PALPACION,
					PERCUSION, AUSCULTACION, COMB.) <button id="j1">Ver</button>
					<button id="j2">Ocultar</button></legend>
					<div id="j">
				<table>
					<tr>
						<td>1) Cabeza:</td>
						<td><textarea rows="5" cols="90" name="cabeza"></textarea></td>
					</tr>

					<tr>
						<td>2) Cuello:</td>
						<td><textarea rows="5" cols="90" name="cuello"></textarea></td>
					</tr>

					<tr>
						<td>3) Torax:</td>
						<td><textarea rows="5" cols="90" name="torax"></textarea></td>
					</tr>

					<tr>
						<td>4) Abdomen:</td>
						<td><textarea rows="5" cols="90" name="abdomen"></textarea></td>
					</tr>

					<tr>
						<td>5) Miembros:</td>
						<td><textarea rows="5" cols="90" name="miembros"></textarea></td>
					</tr>

					<tr>
						<td>6) Genitales:</td>
						<td><textarea rows="5" cols="90" name="genes"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>11) CONCLUSIONES <button id="k1">Ver</button>
					<button id="k2">Ocultar</button></legend>
				<div id="k">
				<table>
					<tr>
						<td>Comentario:</td>
						<td><textarea rows="5" cols="100" name="comentario"></textarea></td>
					</tr>

					<tr>
						<td>Diagnostico:</td>
						<td><textarea rows="5" cols="100" name="diagnostico"></textarea></td>
					</tr>

					<tr>
						<td>Pronostico:</td>
						<td><textarea rows="5" cols="100" name="pronostico"></textarea></td>
					</tr>

					<tr>
						<td>Tratamiento:</td>
						<td><textarea rows="5" cols="100" name="tratamiento"></textarea></td>
					</tr>
				</table>
				</div>
			</fieldset>

			<br> <input type="hidden" name="action" value="create" /> <input
				type="reset" value="Limpiar Historia"> <input type="submit"
				value="Guardar Historia y crear Receta Medica">

		</form>

		<address>Policlinico Health Life</address>
		<a href="http://jigsaw.w3.org/css-validator/check/referer"> <img
			style="border: 0; width: 88px; height: 31px"
			src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" />
		</a>

		<p>
			<a href="http://jigsaw.w3.org/css-validator/check/referer"> <img
				style="border: 0; width: 88px; height: 31px"
				src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
				alt="Valid CSS!" />
			</a>
	</div>
</body>
</html>