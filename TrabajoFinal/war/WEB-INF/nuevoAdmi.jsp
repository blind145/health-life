<%@page import="java.util.Date"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="epis.unsa.beans.*" %>
<%
	TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String date = sdf.format(new Date());
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>

	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>

	<div class="body">
		<h2>Registro para crear un nuevo administrador</h2>
		<hr>
		<form action="/registrarAdmi" method="post">
			<table>
				<tr>
					<td>Nombre(s) *:</td>
					<td><input type="text" name="nombre" required pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)" title="Ingrese solo letras"></td>

					<td>Apellido Paterno *:</td>
					<td><input type="text" name="apellidop" required pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)" title="Ingrese solo letras"></td>

				</tr>

				<tr>
					<td>Apellido Materno *:</td>
					<td><input type="text" name="apellidom" required pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)" title="Ingrese solo letras"></td>

					<td>Fecha de Nacimiento *:</td>
					<td><input type="number"  name="dia" min="1" max="31"placeholder="dd" required title="Ingrese solo numeros">
					 <input  type="number" name="mes" min="1" max="12" placeholder="mm" required title="Ingrese solo numeros"> 
					 <input type="number" name="anio" min="1900" max="2015" placeholder="aaaa" required title="Ingrese solo numeros"></td>
				</tr>

				<tr>
					<td>Estado Civil *:</td>
					<td><input required type="radio" name="estado" value="Soltero" >Soltero
						<input type="radio" name="estado" value="Casado">Casado <input
						type="radio" name="estado" value="Viudo">Viudo <input
						type="radio" name="estado" value="Divorciado" >Divorciado</td>
				</tr>

				<tr>
				<td>Sexo *:</td>
				<td><input required type="radio" name="sexo" value="Masculino">Masculino <input
						type="radio" name="sexo" value="Femenino">Femenino</td>
					<td>DNI *:</td>
					<td><input type="text" name="dni" maxlength="8" required pattern="[0-9]{8}" title="Ingrese solo numeros"></td>
				
				</tr>

				<tr>
				
					<td>Direccion:</td>
					<td><input type="text" name="direccion" pattern="([a-zA-Z�����]*+[\s]*[a-zA-Z�����]*)"></td>
					<td>Manzana / Numero:</td>
					<td><input type="text" name="numero" pattern="([a-zA-Z�����]+[\s]*+[\-]*+[\s]*[a-zA-Z�����]*+[0-9]*)"></td>
					
				</tr>

				<tr>
				
					<td>Distrito:</td>
					<td><input type="text" name="distrito" pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)"></td>
					<td>Provincia:</td>
					<td><input type="text" name="provincia" pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)"></td>
					
				</tr>

				<tr>
				
					<td>Departamento:</td>
					<td><input type="text" name="departamento" pattern="([a-zA-Z�����]+[\s]*[a-zA-Z�����]*)"></td>
					<td>Lugar de Origen:</td>
					<td><input type="text" name="lugar"></td>
					
				</tr>

				<tr>
					<td>Telefono de casa o celular: *</td>
					<td><input required type="tel" name="telefono" pattern="[0-9]{6}|[0-9]{9}"></td>
					<td>Email *:</td>
					<td><input type="email" name="email"
						placeholder="ejemplo@ejemplo.com" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Ingrese un email correcto"></td>
				</tr>
				<tr>
					<td>Fecha de creacion:</td>
					<td><input readonly type="text" name="fecha"
						value="<%=date%>" /></td>	
				</tr>

			</table>
			<br> <input type="hidden" name="action" value="create" /> <input
				type="reset" value="Limpiar Registro"> <input type="submit"
				value="Guardar Registro"> <br>

		</form>
		<i>( *): Campos obligatorios</i>
		<address>Policlinico Health Life</address>
		   <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" />
    </a>
</p>

<p>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valid CSS!" />
    </a>
	</div>
</body>
</html>