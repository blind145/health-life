<%@page import="epis.unsa.*" %>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="epis.unsa.beans.*" %>
<% List<Especialidad> especialidades = (List<Especialidad>)request.getAttribute("verEspecialidades");%>
<%
TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
SimpleDateFormat sdf = new SimpleDateFormat();
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>
	
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	
	<div class="body">
		<h2>Lista de todas las especialidades</h2>
		<hr>
		
		<table id="t01" class= "table">
			<tr>
				<th>N�</th>
				<th>Especialidad</th>
				<th>Turnos de atencion</th>
				<th>Tipo de especialidad</th>
				<th>Fecha de registro</th>
			</tr>
			
			<%for (int i = 0; i < especialidades.size(); i++) {
    Especialidad especialidad = (Especialidad)especialidades.get(i);%>
			
			<tr>
				<td><%= especialidad.getId() %></td>
				<td><%= especialidad.getNombre() %></td>
				<td> <%= especialidad.getTurno() %></td>
				<td><%= especialidad.getTipo() %></td>
				<td><%= sdf.format(especialidad.getFecha()) %></td>
			</tr>
			
				<%}%>
			
		</table>
		
		<address>Policlinico Health Life</address>
		   <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" />
    </a>
</p>

<p>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valid CSS!" />
    </a>
	</div>
</body>
</html>