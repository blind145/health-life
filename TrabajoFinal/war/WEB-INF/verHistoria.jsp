<%@page import="epis.unsa.*" %>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="epis.unsa.beans.*" %>
<% 
List<Historia> historias = (List<Historia>)request.getAttribute("historias");
String historia = (String)request.getAttribute("historia");
%>
<%
TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
SimpleDateFormat sdf = new SimpleDateFormat();
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	<div class="body">
		<h2>Historia Clinica</h2>
		<hr>
		<fieldset>
			<legend>1) FICHA DE IDENTIFICACION</legend>
			<table>

				<tr>
					<td>H. C. Numero:</td>
					<td><input readonly type="text" name="numero" maxlength="7" value="<%= request.getAttribute("numero")%>">
					<td>Internado (Hospitalizado):</td>
					<td><input type="text" name="interno" value= "<%=request.getAttribute("interno")%>"></td>
				</tr>
				
				<tr>
					<td>Nombre(s):</td>
					<td><input readonly type="text" name="nombre" value= "<%=request.getAttribute("nombre")%>"></td>
					<td>Apellido Paterno:</td>
					<td><input readonly type="text" name="ap" value= "<%=request.getAttribute("apellidop")%>"></td>
				</tr>

				<tr>
					<td>Apellido Materno:</td>
					<td><input readonly type="text" name="am" value= "<%=request.getAttribute("apellidom")%>"></td>
					<td>Fecha de Nacimiento:</td>
					<td><input readonly type="text" name="fechanac" maxlength="2" value="<%=request.getAttribute("fechanac")%>" ></td>
				</tr>

				<tr>
					<td>Estado Civil:</td>
					<td><input readonly type="text" name="estado" maxlength="2" value="<%=request.getAttribute("estado")%>" ></td>
					<td>Ocupacion:</td>
					<td><input readonly type="text" name="ocupacion" value="<%=request.getAttribute("ocupacion")%>"></td>
				</tr>

				<tr>
					<td>Grupo Sanguineo:</td>
					<td><input readonly type="text" name="grupo" maxlength="2" value="<%=request.getAttribute("grupo")%>" ></td>
					<td>Sexo:</td>
					<td><input readonly type="text" name="sexo" maxlength="2" value="<%=request.getAttribute("sexo")%>" ></td>
				</tr>
				
				<tr>
					<td>Lugar de residencia:</td>
					<td><input readonly type="text" name="residencia" value= "<%=request.getAttribute("direccion")%>"></td>
					<td>Lugar de Origen:</td>
					<td><input readonly type="text" name="lugarOrigen" value= "<%=request.getAttribute("lugar")%>"></td>
				</tr>

				<tr>
					<td>DNI:</td>
					<td><input readonly type="text" name="dni" maxlength="8" value= "<%=request.getAttribute("dni")%>"></td>
					<td>Fecha de ingreso/consulta:</td>
					<td><input readonly type="text" name="fecha" maxlength="10" value="<%=request.getAttribute("fecha")%>" ></td>
				</tr>

				<tr>
					<td>Talla:</td>
					<td><input readonly type="text" name="talla" maxlength="6" value= "<%=request.getAttribute("talla")%>">
					<td>Peso:</td>
					<td><input readonly type="text" name="peso" maxlength="8" value = "<%=request.getAttribute("peso")%>">
				</tr>

				<tr>
					<td>Persona Responsable:</td>
					<td><input readonly type="text" name="persona" value = "<%=request.getAttribute("persona")%>"></td>
					<td>Relacion con el paciente:</td>
					<td><input readonly type="text" name="relacion" value = "<%=request.getAttribute("relacion")%>"></td>
				</tr>

				<tr>
					<td>Medico Responsable:</td>
					<td><input readonly type="text" name="medico" value = "<%=request.getAttribute("medico")%>"></td>
					<td>Especialidad:</td>
					<td><input readonly type="text" name="especialidad" value = "<%=request.getAttribute("especialidad")%>"></td>
				</tr>

			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>2) ANTECEDENTES</legend>
			a) Heredo Familiares
			<table>
				<tr>
					<td>Tuberculosis, Diabetes Mellitus, Hipertension, Carcinomas,
						Cardiopatias, Hepatopatias, Nefropatias, Enf.endocrinas, Enf.
						Mentales, Epilepsia, Asma, Enf. Hematologicas.</td>
					<td><textarea readonly rows="7" cols="30" name="her"><%=request.getAttribute("her")%></textarea></td>
				</tr>
			</table>

			b) Personales Patologicos
			<table>
				<tr>
					<td>Transfusiones, Intervenciones Quirurgicas, Traumatismos
						Enf. Infecciosas de la infancia, Tb , Enf. Venereas, Fiebre
						Tifoidea, neumonias, Enf. Alergicas, Pad. Articulares</td>
					<td><textarea readonly rows="7" cols="30" name="pat"><%=request.getAttribute("pat")%></textarea></td>
				</tr>
			</table>

			c) Personales no patologicos

			<table>
				<tr>
					<td>Adicciones:</td>
					<td><input readonly type="text" name="adiccion" size="50" value="<%=request.getAttribute("adiccion")%>"></td>
						<td>Otros:</td> 
						<td><input readonly type="text" name="descripcion" size="50" value="<%=request.getAttribute("descripcion")%>"></td>
				</tr>

				<tr>
					<td>Tipo de alimentacion:</td>
					<td><textarea readonly rows="5" cols="30" name="alimentacion"><%=request.getAttribute("alimentacion")%></textarea></td>
					<td>Deportes:</td>
					<td><textarea readonly rows="5" cols="30" name="deportes"><%=request.getAttribute("deportes")%></textarea></td>
				</tr>

				<tr>
					<td>Inmunizaciones:</td>
					<td><textarea readonly rows="5" cols="30" name="inmune"><%=request.getAttribute("inmune")%></textarea></td>
					<td>Alergias:</td>
					<td><textarea readonly rows="5" cols="30" name="alergias"><%=request.getAttribute("alergias")%></textarea></td>
				</tr>
			</table>
		</fieldset>
<br>

		<fieldset>
			<legend>3) PADECIMIENTO ACTUAL (1 PRINCIPIO, 2 EVOLUCION, 3
				ESTADO ACTUAL)</legend>
			<table>
				<tr>
					<td><textarea readonly rows="5" cols="115" name="padec"><%=request.getAttribute("padec")%></textarea></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>4) SINTOMAS GENERALES </legend>

			<table>
				<tr>
					<td><input readonly type="text" name="adiccion" size="50" value="<%=request.getAttribute("sintoma")%>"></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>5) INTERROGATORIO POR APARATOS Y SISTEMAS</legend>
			<table>
				<tr>
					<td><ins>Aparato digestivo.</ins> Nausea, vomito,
						(hematemesis), dolor abd. meteorismo,constipacion, diarrea</td>
					<td><textarea readonly rows="7" cols="30" name="diges"><%=request.getAttribute("diges")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Aparato cardiovascular.</ins> Disnea, tos (seca.
						prod.), hemoptisis, dolor precordial, palpitaciones, cianosis,
						edema y manifestaciones perifericas (acufenos, fosfenos, sincope,
						lipotimia, cefalea,etc)</td>
					<td><textarea readonly rows="7" cols="30" name="cardio"><%=request.getAttribute("cardio")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Aparato respiratorio.</ins> Tos, disnea, dolor
						toraxico, hemoptisis, cianosis, vomica, alteraciones de la voz.</td>
					<td><textarea readonly rows="7" cols="30" name="resp"><%=request.getAttribute("resp")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Aparato urinario.</ins> Alteraciones de la miccion
						(poliuria, anuria, polaquiuria,oliguria, nicturia, opsiuria,
						disuria, tenesmo vesical, urgencia, chorro, enuresis,
						incontenincia) caracteres de la orina (volumen, olor, color,
						aspecto) dolor lumbar, edema renal, hipertension arterial</td>
					<td><textarea readonly rows="7" cols="30" name="ur"><%=request.getAttribute("ur")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Aparato genital.</ins> Criptorquidia, fimosis,
						funcion sexual. Sangrado genital, flujo o leucorrea, dolor
						ginecologico, prurito vulvar</td>
					<td><textarea readonly rows="7" cols="30" name="gen"><%=request.getAttribute("gen")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Aparato hematologico.</ins> Datos clinicos de anemia
						(palidez, astenia, adinamia y otros), hemorragias, adenopatias,
						esplenomegalia.</td>
					<td><textarea readonly rows="7" cols="30" name="hema"><%=request.getAttribute("hema")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Sistema endocrino.</ins> Bocio, letargia,
						bradipsiquia (lalia), intol. calor/frio, nerviosismo,
						hiperquinesis, carac. sexuales, galactorrea, amenorrea,
						ginecomastia, obesidad, ruborizacion.</td>
					<td><textarea readonly rows="7" cols="30" name="endo"><%=request.getAttribute("endo")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Sistema osteomuscular.</ins> Ganglios, xeroftalmia,
						xerostomia, fotosensibilidad artralgias/mialgias, Raynaud</td>
					<td><textarea readonly rows="7" cols="30" name="osteo"><%=request.getAttribute("osteo")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Sistema nervioso.</ins> cefalea, sincope,
						convulsiones, deficit transitorio, vertigo, confusion y obnub.,
						vigilia/sue�o, paralisis y M, marcha y equilibrio, sensibilidad.</td>
					<td><textarea readonly rows="7" cols="30" name="nerv"><%=request.getAttribute("nerv")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Sistema sensorial.</ins> Vision, agudeza, borrosa
						diplopia, fosgenos, dolor ocular, fotofobia, xeroftalmia,
						amaurosis, otalgia, otorrea y otorragia, hipoacusia, tinitus,
						olfaccion, epistaxis, secrecion</td>
					<td><textarea readonly rows="7" cols="30" name="sens"><%=request.getAttribute("sens")%></textarea></td>
				</tr>

				<tr>
					<td><ins>Psicosomatico</ins> Personalidad, ansiedad,
						depresion, afectividad, emotividad, amnesia, voluntad,
						pensamiento, atencion, ideacion suicida, delirios</td>
					<td><textarea readonly rows="7" cols="30" name="psico"><%=request.getAttribute("psico")%></textarea></td>
				</tr>
			</table>
		</fieldset>

<br>
		<fieldset>
			<legend>6) DIAGNOSTICOS ANTERIORES</legend>
			<table>
				<tr>
					<td><textarea readonly rows="5" cols="115" name="anteriores"><%=request.getAttribute("anteriores")%></textarea></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>7) TERAPEUTICA EMPLEADA ANTERIORMENTE</legend>
			<table>
				<tr>
					<td><textarea readonly rows="5" cols="115" name="terapia"><%=request.getAttribute("terapia")%></textarea></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>8) SIGNOS VITALES</legend>
			<table>
				<tr>
					<td>Frecuencia Cardiaca (FC):</td>
					<td><input readonly type="text" name="fc" value="<%=request.getAttribute("fc")%>"></td>
					<td>Frecuencia Respiratoria (FR):</td>
					<td><input readonly type="text" name="fr" value="<%=request.getAttribute("fr")%>"></td>
				</tr>

				<tr>
					<td>Tension Arterial (TA):</td>
					<td><input readonly type="text" name="ta" value="<%=request.getAttribute("ta")%>"></td>
					<td>Temperatura (T):</td>
					<td><input readonly type="text" name="t" value="<%=request.getAttribute("t")%>"></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>9) EXPLORACION GENERAL</legend>
			<table>
				<tr>
					<td><textarea readonly rows="5" cols="115" name="exploracion"><%=request.getAttribute("exploracion")%></textarea></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>10) EXPLORACION REGIONAL (INSPECCION, PALPACION,
				PERCUSION, AUSCULTACION, COMB.)</legend>
			<table>
				<tr>
					<td>1) Cabeza:</td>
					<td><textarea readonly rows="5" cols="90" name="cabeza"><%=request.getAttribute("cabeza")%></textarea></td>
				</tr>

				<tr>
					<td>2) Cuello:</td>
					<td><textarea readonly rows="5" cols="90" name="cuello"><%=request.getAttribute("cuello")%></textarea></td>
				</tr>

				<tr>
					<td>3) Torax:</td>
					<td><textarea readonly rows="5" cols="90" name="torax"><%=request.getAttribute("torax")%></textarea></td>
				</tr>

				<tr>
					<td>4) Abdomen:</td>
					<td><textarea readonly rows="5" cols="90" name="abdomen"><%=request.getAttribute("abdomen")%></textarea></td>
				</tr>

				<tr>
					<td>5) Miembros:</td>
					<td><textarea readonly rows="5" cols="90" name="miembros"><%=request.getAttribute("miembros")%></textarea></td>
				</tr>

				<tr>
					<td>6) Genitales:</td>
					<td><textarea readonly rows="5" cols="90" name="genes"><%=request.getAttribute("genes")%></textarea></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<fieldset>
			<legend>11) CONCLUSIONES</legend>
			<table>
				<tr>
					<td>Comentario:</td>
					<td><textarea readonly rows="5" cols="100" name="comentario"><%=request.getAttribute("comentario")%></textarea></td>
				</tr>

				<tr>
					<td>Diagnostico:</td>
					<td><textarea readonly rows="5" cols="100" name="diagnostico"><%=request.getAttribute("diagnostico")%></textarea></td>
				</tr>

				<tr>
					<td>Pronostico:</td>
					<td><textarea readonly rows="5" cols="100" name="pronostico"><%=request.getAttribute("pronostico")%></textarea></td>
				</tr>

				<tr>
					<td>Tratamiento:</td>
					<td><textarea readonly rows="5" cols="100" name="tratamiento"><%=request.getAttribute("tratamiento")%></textarea></td>
				</tr>
			</table>
		</fieldset>

		<address>Policlinico Health Life</address>
		   <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" />
    </a>
</p>

<p>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Valid CSS!" />
    </a>
	</div>
</body>
</html>