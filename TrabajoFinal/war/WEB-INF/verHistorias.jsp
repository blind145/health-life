<%@page import="epis.unsa.*" %>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="epis.unsa.beans.*" %>
<% List<Historia> historias = (List<Historia>)request.getAttribute("verHistorias");%>
<%
TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
SimpleDateFormat sdf = new SimpleDateFormat();
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" src="logo.png">
			</a></th>

			<th>
				<h1>SISTEMA DE ADMINISTRACION DE HISTORIAS CLINICAS</h1>
			</th>
			<th><a class="cerrar" href="cerrar">CERRAR SESION</a></th>
		</tr>
	</table>
	
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul>
			</li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR ESPECIALIDADES</a>
				</ul>
			</li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR ADMINISTRADORES</a>
				</ul>
			</li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER
							HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR HISTORIA</a>
				</ul>
			</li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	
	<div class="body">
		<h2>Historias clinicas registradas</h2>
		<hr>

		<table id="t01" class= "table">
			<tr>
				<th>Numero de Historia</th>
				<th>DNI</th>
				<th>Medico</th>
				<th>Especialidad</th>
				<th>Fecha</th>
				<th>Receta</th>
			</tr>
			
			<%for (int i = 0; i < historias.size(); i++) {
    Historia historia = (Historia)historias.get(i);%>
			
			<tr>
				<td><a href="/verHistoria?action=<%=historia.getNumero()%>"><%= historia.getNumero() %></a></td>
				<td><%= historia.getDNI() %></td>
				<td><%= historia.getMedico() %></td>
				<td><%= historia.getEspecialidad() %></td>
				<td><%= sdf.format(historia.getFecha()) %></td>
				<td><a href="/verReceta?action=<%=historia.getNumero()%>">ver Receta</a></td>
			</tr>
			<%}%>
		</table>
					
		<address>Policlinico Health Life</address>
	</div>
</body>
</html>