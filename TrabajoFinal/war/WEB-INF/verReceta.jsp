<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="epis.unsa.beans.*" %>
<%
	TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String date = sdf.format(new Date());
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Health Life</title>
<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
</head>
<body>
	<table>
		<tr>
			<th><a href="gmail"> <img class="img" alt="logo"
					src="/logo.png"></a></th>

			<th class="titulo">SISTEMA DE ADMINISTRACION DE HISTORIAS
				CLINICAS</th>
			<th><a href="cerrar"> Cerrar sesion </a></th>
		</tr>
	</table>
	<div id="header">
		<ul class="nav">
			<li><a href="gmail">INICIO</a></li>
			<li><a>PACIENTES</a>
				<ul>
					<li><a href="registrarPaciente?action=show">VER PACIENTES</a></li>
					<li><a href="registrarPaciente?action=red">AGREGAR
							PACIENTE</a>
					<li><a href="registrarPaciente?action=borrar">BORRAR
							PACIENTES</a>
				</ul></li>
			<li><a>MEDICOS</a>
				<ul>
					<li><a href="/registrarMedico?action=show">VER MEDICOS</a></li>
					<li><a href="registrarMedico?action=red">AGREGAR MEDICO</a>
					<li><a href="registrarMedico?action=borrar">BORRAR MEDICOS</a>
				</ul></li>
			<li><a>ESPECIALIDADES</a>
				<ul>
					<li><a href="/registrarEspecialidad?action=show">VER
							ESPECIALIDADES</a></li>
					<li><a href="registrarEspecialidad?action=red">AGREGAR
							ESPECIALIDAD</a>
					<li><a href="registrarEspecialidad?action=borrar">BORRAR
							ESPECIALIDADES</a>
				</ul></li>
			<li><a>ADMINISTRADORES</a>
				<ul>
					<li><a href="/registrarAdmi?action=show">VER
							ADMINISTRADORES</a></li>
					<li><a href="registrarAdmi?action=red">AGREGAR
							ADMINISTRADOR</a>
					<li><a href="registrarAdmi?action=borrar">BORRAR
							ADMINISTRADORES</a>
				</ul></li>
			<li><a>HISTORIAS</a>
				<ul>
					<li><a href="/registrarHistoria?action=show">VER HISTORIAS</a></li>
					<li><a href="/registrarHistoria?action=buscar">BUSCAR
							HISTORIAS</a>
					<li><a href="/registrarHistoria?action=red">AGREGAR
							HISTORIA</a>
				</ul></li>
			<li><a href="/buscarHistorias?action=mapa">MAPA DE SITIO</a></li>
		</ul>
	</div>
	<div class="body">
	
		<h2>Receta medica</h2>
		<hr>
		<table>

			<tr>
				<td><img src="logo.png" height="100" width="150" /></td>
				<td>CENTRO MEDICO HEALTH LIFE</td>
				<td><img src="logo.png" height="100" width="150" /></td>
			</tr>
</table>
<br>
<table>
			<tr>
				<td>H. C. Numero:</td>
				<td><input readonly type="text" name="numero"
					value="<%=request.getAttribute("numero")%>"></td>
				<td>Consulta Numero:</td>
				<td><input readonly type="text" name="numeroc"
					value="<%=request.getAttribute("numeroc")%>"></td>
			</tr>

			<tr>
				<td>Nombres y Apellidos:</td>
				<td><input readonly type="text" name="paciente"
					value="<%=request.getAttribute("paciente")%>"></td>
				<td>Fecha:</td>
				<td><input readonly type="text" name="fecha" value="<%=request.getAttribute("fecha")%>" /></td>
			</tr>
		</table>
		<br>
		<table>	
			<tr>
			<td>Rp:</td>
			
			<td>Indicaciones:</td>
			</tr>
			
			<tr>
			<td><textarea readonly rows="17" cols="50" name="rp"><%=request.getAttribute("rp")%></textarea></td>
			
			<td><textarea readonly rows="17" cols="50" name="indic"><%=request.getAttribute("indic")%></textarea></td>
			</tr>
	</table>
	<br>
	<table>		
			<tr>
			<td>Medico:</td>
			<td><input readonly type="text" name="medico"
					value="<%=request.getAttribute("medico")%>"></td>
			
			</tr>

		</table>
		
	
<p>* Receta Original para pacientes, salvo psicotropicos.
		<address>
			Policlinico Health Life
			<p>
				<a href="http://jigsaw.w3.org/css-validator/check/referer"> <img
					style="border: 0; width: 88px; height: 31px"
					src="http://jigsaw.w3.org/css-validator/images/vcss"
					alt="Valid CSS!" />
				</a>
			</p>

			<p>
				<a href="http://jigsaw.w3.org/css-validator/check/referer"> <img
					style="border: 0; width: 88px; height: 31px"
					src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
					alt="Valid CSS!" />
				</a>
			</p>
		</address>
	</div>
</body>
</html>
